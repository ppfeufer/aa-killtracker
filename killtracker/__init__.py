""""An app for running killmail trackers with Alliance Auth and Discord."""

# pylint: disable = invalid-name
default_app_config = "killtracker.apps.KillmailsConfig"

__version__ = "0.15.0"
__title__ = "Killtracker"

APP_NAME = "aa-killtracker"
HOMEPAGE_URL = "https://gitlab.com/ErikKalkoken/aa-killtracker"
USER_AGENT_TEXT = f"{APP_NAME} v{__version__}"
